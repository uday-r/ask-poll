var _       = require('lodash');

var PollService = function() {

  var polls = [
    {
      _id: 321,
      question: 'Which hotel?',
      url: 'ask-poll/hotels',
      impressions: []
    },
    {
      _id: 444,
      question: 'Where is the nucleus?',
      url: 'ask-poll/cell',
      impressions: []
    }
  ];

  return {

    getAllPolls: function() {
      return polls;
    },

    getValidPoll: function(pollId) {
      var currentPoll = _.find(polls, function(itr) {
        return itr._id == pollId;
      });
      if(!currentPoll) {
        client.emit('ERROR', {
          level: 'CRITICAL',
          description: 'Could not set impression, please try again.'
        });
      }
      return currentPoll;
    },
    addNewPoll: function(poll) {
      polls.push(poll);
    }
  }
}

var pollService = new PollService();

getPollService = function() {
  return pollService;
}
