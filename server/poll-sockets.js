var socket = require('socket.io');
var _       = require('lodash');

require('./poll-service.js');


pollService = getPollService();
initSocket = function(server) {

  var io = socket.listen(server);

  io.sockets.on('connection', function (client) {

    client.emit('ALL.POLLS', pollService.getAllPolls());

    client.on('ENTER.POLL', function(data) {
      client.join(data.pollId);
      this.userId = data.user._id;
      this.pollId = data.pollId;
    });

    client.on('ADD.IMPRESSION', function(data) {
      var currentPoll = pollService.getValidPoll(data.pollId);
      currentPoll.impressions = currentPoll.impressions? currentPoll.impressions: [];
      currentPoll.impressions.push({
        user: data.user,
        impression: data.impression
      });
      client.broadcast
        .to(data.pollId).emit('NEW.IMPRESSION', {
          user: data.user,
          impression: data.impression
        });
    });


    client.on('REMOVE.IMPRESSION', function(data) {

      var currentPoll = pollService.getValidPoll(data.pollId);
      _.remove(currentPoll.impressions, function(itr) {
        return itr.user._id == data.userId;
      });
    });

    client.on('MODIFY.IMPRESSION', function(data) {

      var currentPoll = pollService.getValidPoll(data.pollId);
      currentPoll.impressions = currentPoll.impressions? currentPoll.impressions: [];
      _.remove(currentPoll.impressions, function(itr) {
        return itr.user._id == data.user._id;
      });
      currentPoll.impressions.push({
        user: data.user,
        impression: data.impression
      });
      client.broadcast
        .to(data.pollId).emit('EXISTING.IMPRESSION', {
          user: data.user,
          impression: data.impression
        });
    });

    client.on('disconnect', function(data) {
      console.log('Client disconnect');
      var _this = this;
      var currentPoll = pollService.getValidPoll(_this.pollId);
      if(currentPoll) {
        _.remove(currentPoll.impressions, function(itr) {
          return itr.user._id == _this.userId;
        });
      }
      client.broadcast
        .to(_this.pollId).emit('DELETE.IMPRESSION', {
          userId: _this.userId
        });
      client.leave(this.pollId);
    });


  });

}
