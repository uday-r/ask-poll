var bodyParser = require('body-parser');
require('./poll-service.js');

pollService = getPollService();
routes = function(app) {
  app.use(bodyParser());
  app.get('/api/polls', function(req, res) {
    res.status(200).type('json').send(pollService.getAllPolls());
  });
  app.post('/api/newPoll', function(req, res) {
    var newPoll = req.body;
    pollService.addNewPoll(newPoll);
    res.status(200).type('json').send(pollService.getAllPolls());
  });
}
