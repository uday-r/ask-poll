var express = require('express');
var app     = express();
var server  = app.listen(process.env.PORT || 8089);
//var pollSockets = require('./server/poll-sockets.js');
require('./server/routes.js');
require('./server/poll-sockets.js');

app.use(express.static(__dirname + '/app/'));

routes(app);

app.use('/*', function(req, res){
    res.sendFile(__dirname + '/app/index.html');
});

server.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});

initSocket(server);
