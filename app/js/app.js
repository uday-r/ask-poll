(function(){
  'use strict';

  angular
    .module('hyp.app', [
      'ngMaterial',
      'ngAnimate',
      'ngMessages',
      'ngAria',
      'btford.socket-io',
      'hyp.pages.home',
      'angular-cloudinary', 
      'ngFileUpload',
      'vm.core',
      'vm.polls'
    ])
    .factory('appStorage', [
      function() {
        return localStorage;
      }
    ])
    .factory('Socket', [
      'socketFactory', '$location', function(socketFactory, $location) {
        return socketFactory();
      }
    ])
    .config([
        '$mdThemingProvider', function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
          .primaryPalette('deep-purple', {
            'default': '500',
            'hue-1': '700',
            'hue-2': '300',
            'hue-3': '50'
          }).accentPalette('green', {
            'default': '500',
            'hue-1': '700',
            'hue-2': 'A200'
          }).warnPalette('red', {
            'default': 'A700',
            'hue-1': '500',
            'hue-2': '300',
            'hue-3': '400'
          });

      }
    ])
    .controller('hyp.main', [
      '$rootScope',
      '$scope',
      function($rootScope, $scope) {
      }
    ]);
})();
