(function(){
  'use strict';

  angular
    .module('vm.core')
    .factory('clusterService', ['$q', function($q) {

      const distanceBt = function(p1, p2) {
        const xSq = Math.pow(p1[0] - p2[0], 2);
        const ySq = Math.pow(p1[1] - p2[1], 2);
        return Math.sqrt(xSq + ySq);
      }

      return {
        getMaxCluster: function(nodes) {
          const clusterGroups = getClusters(nodes);
          const biggestCluster = _.maxBy(clusterGroups, function(itr) {
            return itr.data.length;
          });

          const maxCordinate = _.maxBy(biggestCluster.data, function(itr) {
            return distanceBt(biggestCluster.mean, itr);
          });

          var diameter = 2 * distanceBt(biggestCluster.mean, maxCordinate);

          return {
            center: {
              x: biggestCluster.mean[0],
              y: biggestCluster.mean[1]
            },
            diameter: diameter
          }

        }
      }
    }
    ]);
})();
