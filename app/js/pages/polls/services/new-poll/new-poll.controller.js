(function(){
  'use strict';
  angular
    .module('vm.polls')
    .controller('vmNewPollCtrl', [
      '$scope', 'appStorage', '$mdDialog', 'Upload', 'PollService', 'polls',
       function($scope, appStorage, $mdDialog, Upload, pollService, polls) {


         $scope.upload = function(file) {
           if(!file || file.length == 0) {
             return;
           }
           $scope.loading = true;
           Upload.upload({
             url: "https://api.cloudinary.com/v1_1/middle-maan/upload",
             data: {
               'file': file[0],
               upload_preset: 'ask-poll',
             }
           }).progress(function (e) {
             $scope.uploadProgress = Math.round((e.loaded * 100.0) / e.total);
           }).success(function (data, status, headers, config) {
             $scope.uploadedImage = data.url;
             $scope.uploadedPublicId = data.public_id;
             $scope.loading = false;
           }).error(function (data, status, headers, config) {
             $scope.loading = false;
             console.log(data);
           });
         }

         $scope.save = function() {
           var newPoll = {
             _id: Date.now(),
             question: $scope.question,
             url: $scope.uploadedPublicId,
             impressions: []
           };
           $scope.loading = true;
           pollService
             .newPoll(newPoll)
             .then(function(res) {
               $scope.loading = false;
               polls.push(newPoll);
             })
             .catch(function(err) {
               $scope.loading = false;
             });
           $mdDialog.hide();
         }

         $scope.cancel = function() {
           $mdDialog.cancel();
         }

       }
    ]);
})();
