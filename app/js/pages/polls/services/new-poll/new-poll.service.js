(function(){
  'use strict';

  angular
    .module('vm.polls')
    .factory('newPoll', ['$q', '$mdDialog', '$mdMedia', function($q, $mdDialog, $mdMedia) {
      return {
        addNewPoll: function(polls) {
          $mdDialog.show({
            controller: 'vmNewPollCtrl',
            templateUrl: '/js/pages/polls/services/new-poll/new-poll.view.html',
            clickOutsideToClose:false,
            escapeToClose: false,
            fullscreen: $mdMedia('xs'),
            locals: {
              polls: polls
            }
          }).then(function(loggedIn) {
          }).catch(function(fail) {
          });
        }
      }
    }
    ]);
})();
