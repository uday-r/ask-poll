(function(){
  'use strict';

  angular
    .module('vm.polls')
    .factory('Loading',['$mdDialog', '$mdMedia', function ($mdDialog, $mdMedia) {

      return {
        start: function() {
          $mdDialog.show({
            templateUrl: '/js/pages/polls/services/loading/loading.view.html',
          });
        },
        stop: function() {
          $mdDialog.hide();
        }
      };
    }]);

})();
