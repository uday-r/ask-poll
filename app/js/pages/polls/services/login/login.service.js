(function(){
  'use strict';

  angular
    .module('vm.polls')
    .factory('loginService', ['$q', '$mdDialog', 'appStorage', function($q, $mdDialog, appStorage) {
      return {
        loginIfNot: function(currentUser) {
          if(appStorage.getItem('userName')) {
            return false;
          }
          $mdDialog.show({
            controller: 'vmLoginCtrl',
            templateUrl: '/js/pages/polls/services/login/login.view.html',
            clickOutsideToClose:false,
            escapeToClose: false,
          }).then(function(loggedIn) {
            currentUser._id = appStorage.getItem('userId');
            currentUser.name = appStorage.getItem('userName');
          }).catch(function(fail) {
          });
        },
        changeName: function(currentUser) {
          $mdDialog.show({
            controller: 'vmLoginCtrl',
            templateUrl: '/js/pages/polls/services/login/login.view.html',
            clickOutsideToClose:false,
            escapeToClose: false,
          }).then(function(loggedIn) {
            currentUser._id = appStorage.getItem('userId');
            currentUser.name = appStorage.getItem('userName');
          }).catch(function(fail) {
          });
        }

      }
    }
    ]);
})();
