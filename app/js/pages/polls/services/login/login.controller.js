(function(){
  'use strict';
  angular
    .module('vm.polls')
    .controller('vmLoginCtrl', [
      '$scope', 'appStorage', '$mdDialog',
       function($scope, appStorage, $mdDialog) {

         $scope.name = appStorage.getItem('userName');
         $scope.save = function() {
           appStorage.setItem('userName', $scope.name);
           appStorage.setItem('userId', Date.now());
           $mdDialog.hide();
         }

       }
    ]);
})();
