(function(){
  'use strict';

  angular
    .module('vm.polls')
    .factory('PollService',['Loading', '$q', '$http', function(loading, $q, $http) {

      return {
        list: function() {
          var defer = $q.defer();
          loading.start();
          $http.get('/api/polls')
            .then(function(resp) {
              defer.resolve(resp.data);
              loading.stop();
            })
            .catch(function(err) {
              defer.reject()
              loading.stop();
            });
          return defer.promise;
        },
        newPoll: function(poll) {
          return $http.post('/api/newPoll', poll);
        }
      };
    }]);

})();
