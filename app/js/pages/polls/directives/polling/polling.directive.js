(function(){
  'use strict';
  angular
    .module('vm.polls')
    .directive('vmPolling', ['$location',
      function($location) {
        return {
          restrict: 'E',
          replace: true,
          templateUrl: './js/pages/polls/directives/polling/polling.view.html',
          scope: false,
          link: function(scope, el, attrs, ctrl) {
            $("#image-container > img").one("load", function() {
              scope.$apply(function() {
                scope.imgWidth = $('#image-container > img').width();
              });
            });
            el.find('img').on('click', function(e) {
              var xPos = e.clientX - e.target.getBoundingClientRect().left;
              var yPos = e.clientY - e.target.getBoundingClientRect().top;
              var totalHeight = e.target.height;
              var totalWidth = e.target.width;
              scope.$apply(function() {
                scope.relativePos = {
                  x: (xPos * 100)/totalWidth,
                  y: (yPos * 100)/totalHeight
                }
              });
            });
          },
          controller: ['$rootScope', '$scope', '$location', '$route', '$routeParams', 
            'Socket', 'appStorage', 'loginService', '$mdToast', '$mdSidenav', 'clusterService',
            '$mdMedia',
            function($rootScope, $scope, $location, $route, $routeParams, 
              Socket, appStorage, loginService, $mdToast, $mdSidenav, clusterService,
              $mdMedia) {

              $scope.$mdMedia = $mdMedia;
              if($rootScope.wasHere) {
                $rootScope.wasHere = false;
                window.location.reload();
              }
              $rootScope.wasHere = true;

              loginService.loginIfNot();

              $scope.changeName = function() {
                loginService.changeName($scope.currentUser);
              }

              $scope.currentUser = {
                _id: appStorage.getItem('userId'),
                name: appStorage.getItem('userName')
              }

              const showToast = function(msg) {
                $mdToast.show(
                  $mdToast.simple()
                  .textContent(msg)
                  .position("top right"));
              }

              Socket.emit('ENTER.POLL', {
                pollId: $routeParams.pollId,
                user: {
                  _id: appStorage.getItem('userId'),
                  name: appStorage.getItem('userName'),
                }
              });

              const getCurrentPoll = function(polls, pollId) {
                return _.find(polls, function(itr) {
                  return itr._id == pollId;
                });
              }

              Socket.on('ALL.POLLS', function(polls) {
                $scope.polls = polls;
                $scope.poll = getCurrentPoll($scope.polls, $routeParams.pollId);
                if(!$scope.poll) {
                  $location.path('/');
                }
              });

              Socket.on('NEW.IMPRESSION', function(impression) {
                $scope.poll.impressions.push(impression);
              });

              Socket.on('EXISTING.IMPRESSION', function(impression) {
                _.remove($scope.poll.impressions, function(it) {
                  return it.user._id == impression.user._id;
                });
                $scope.poll.impressions.push(impression);
              });

              Socket.on('DELETE.IMPRESSION', function(data) {
                _.remove($scope.poll.impressions, function(it) {
                  return it.user._id == data.userId;
                });
              });

              $scope.$watch('relativePos', function(newPosition) {
                if(!newPosition) {
                  return;
                }

                var myImpressions = _.remove($scope.poll.impressions, function(itr) {
                  return itr.user._id == appStorage.getItem('userId');
                });
                $scope.poll.impressions.push({
                  user: {
                    _id: appStorage.getItem('userId'),
                    name: appStorage.getItem('userName'),
                  },
                  impression: newPosition
                });

                if(!myImpressions || myImpressions.length == 0) {
                  Socket.emit('ADD.IMPRESSION', {
                    pollId: $scope.poll._id,
                    user: {
                      _id: appStorage.getItem('userId'),
                      name: appStorage.getItem('userName'),
                    },
                    impression: newPosition
                  });
                  return;
                }
                Socket.emit('MODIFY.IMPRESSION', {
                  pollId: $scope.poll._id,
                  user: {
                    _id: appStorage.getItem('userId'),
                    name: appStorage.getItem('userName'),
                  },
                  impression: newPosition
                });
              });


              $scope.markBiggestCluster = function() {
                var impArr = $scope.poll.impressions.map(
                  function(imp) {
                    return [imp.impression.x, imp.impression.y];
                  }
                );
                $scope.region = clusterService.getMaxCluster(impArr);
                $scope.region.center.x = $scope.region.center.x - ($scope.region.diameter/2);
                $scope.region.center.y = $scope.region.center.y - ($scope.region.diameter/2);
                $scope.region.diameter = (($scope.region.diameter * $scope.imgWidth)/100) + 40 ;
              }

            }
          ]
        }
      }
    ]);
})();
