(function(){
  'use strict';
  angular
    .module('vm.polls')
    .directive('vmPollSidebar', ['$location',
      function($location) {
        return {
          restrict: 'E',
          templateUrl: './js/pages/polls/directives/poll-sidebar/poll-sidebar.view.html',
          scope: false,
          link: function(scope, el, attrs, ctrl) {
          },
          controller: ['$rootScope', '$scope', '$location', '$routeParams', 
            'Socket', 'appStorage', 'loginService', '$mdToast', '$mdSidenav', 'clusterService',
            function($rootScope, $scope, $location, $routeParams, 
              Socket, appStorage, loginService, $mdToast, $mdSidenav, clusterService) {

              $scope.removeRegion = function() {
                $scope.region = null;
              }

            }
          ]
        }
      }
    ]);
})();
