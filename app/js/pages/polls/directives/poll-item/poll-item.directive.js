(function(){
  'use strict';
  angular
    .module('vm.polls')
    .directive('vmPollItem', ['$location',
      function($location) {
        return {
          restrict: 'E',
          replace: true,
          templateUrl: './js/pages/polls/directives/poll-item/poll-item.view.html',
          scope: false,
          link: function(scope, el) {
            el.on('click', function(e) {
              scope.$apply(function() {
                $location.path(scope.poll._id);
              });
            });
          },
          controller: ['$rootScope', '$scope', '$location',
            function($rootScope, $scope, $location) {

            }
          ]
        }
      }
    ]);
})();
