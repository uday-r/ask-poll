(function(){
  'use strict';
  angular
    .module('vm.polls')
    .directive('vmPollIndicator', [
      function() {
        return {
          restrict: 'E',
          replace: true,
          templateUrl: './js/pages/polls/directives/poll-indicator/poll-indicator.view.html',
          scope: false,
          controller: ['$rootScope', '$scope', 'appStorage',
            function($rootScope, $scope, appStorage) {

              $scope.myImpression = $scope.impression.user._id == appStorage.getItem('userId');
              $scope.currentUserId = appStorage.getItem('userId');
            }
          ]
        }
      }
    ]);
})();
