(function(){
  'use strict';
  angular
    .module('vm.polls')
    .directive('vmPollsListing', [
      function() {
        return {
          restrict: 'E',
          templateUrl: './js/pages/polls/directives/polls-listing/polls-listing.view.html',
          scope: false,
          controller: ['$rootScope', '$scope', 'loginService', 'PollService', 'newPoll', 'appStorage',
            function($rootScope, $scope, loginService, pollService, newPoll, appStorage) {

              $scope.currentUser = {
                _id: appStorage.getItem('userId'),
                name: appStorage.getItem('userName')
              }

              loginService.loginIfNot($scope.currentUser);

              $scope.changeName = function() {
                loginService.changeName($scope.currentUser);
              }

              $scope.$watch('currentUser._id', function(newUser) {
                if(newUser) {
                  pollService
                    .list()
                    .then(function(polls) {
                      $scope.polls = polls;
                    })
                    .catch(function(err) {
                    });
                }
              });


              $scope.uploadPoll = function() {
                newPoll.addNewPoll($scope.polls);
              }

            }
          ]
        }
      }
    ]);
})();
