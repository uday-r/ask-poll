(function(){
  'use strict';

  angular.module('hyp.pages.home')
    .config(['$routeProvider', '$locationProvider', homeRoutes]);

  function homeRoutes($routeProvider, $locationProvider, $q){
    $routeProvider
      .when('/', {
        templateUrl: '/js/pages/home/home.view.html',
      })
      .when('/:pollId', {
        templateUrl: '/js/pages/home/poll.view.html',
      })
    ;

    $locationProvider.html5Mode(true);
  }

})();
